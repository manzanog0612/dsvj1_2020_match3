#include "scenes/gameplay.h"

#include <cmath>
#include <vector>
#include <iostream>

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "elements/elements.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"
#include "sub_scenes/jewels.h"
#include "sub_scenes/characters.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace elements;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;
using namespace jewels;
using namespace characters;

namespace game
{
	namespace gameplay
	{
		static void restartGame()
		{
			if (lose)
				lose = false;
			if (win)
				win = false;
			if (soundPLayed)
				soundPLayed = false;
			if (sound2PLayed)
				sound2PLayed = false;
			gridInitialization();
			charactersInitialization();
		}

		static void drawPauseScreen()
		{
			pause.text = "Pause";
			pressEnter.text = "Press enter to go back to menu";
			pressP.text = "Press P to coninue";
			pressR.text = "Press R to restart the game";
			pressM.text = "Press M to mute audio";

			pause.fontSize = static_cast<int>(80.0f * drawScaleY);
			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressP.fontSize = pressEnter.fontSize;
			pressR.fontSize = pressEnter.fontSize;
			pressM.fontSize = pressEnter.fontSize;

			pause.posY = static_cast<int>(screenHeight) / 2 - pause.fontSize / 2;
			pressEnter.posY = screenLimit.down - pressEnter.fontSize * 2;
			pressP.posY = pause.posY - pressP.fontSize * 3;
			pressM.posY = pause.posY + pause.fontSize + pressP.fontSize * 2;
			pressR.posY = pressM.posY + pressM.fontSize * 2;

			DrawText(pause.text, (screenLimit.right - MeasureText(pause.text, pause.fontSize)) / 2, pause.posY, pause.fontSize, MAGENTA);

			DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, YELLOW);

			DrawText(pressP.text, (screenLimit.right - MeasureText(pressP.text, pressP.fontSize)) / 2, pressP.posY, pressP.fontSize, ORANGE);

			DrawText(pressM.text, (screenLimit.right - MeasureText(pressM.text, pressM.fontSize)) / 2, pressM.posY, pressM.fontSize, ORANGE);

			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2, pressR.posY, pressR.fontSize, ORANGE);
		}

		static void drawLosingScreen()
		{
			loser[0].text = "You lost";
			loser[1].text = "Wanna try again?";
			pressR.text = "Press R to restart the game";
			pressEnter.text = "Press enter to go back to menu";

			loser[0].fontSize = static_cast<int>(55.0f * drawScaleY);
			loser[1].fontSize = static_cast<int>(40.0f * drawScaleY);
			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressR.fontSize = pressEnter.fontSize;
			
			loser[0].posY = static_cast<int>(screenHeight / 2.0f - loser[0].fontSize / 2.0f);
			loser[1].posY = static_cast<int>(loser[0].posY + loser[0].fontSize + loser[1].fontSize / 4.0f);
			pressEnter.posY = screenLimit.down - pressEnter.fontSize * 2;
			pressR.posY = loser[0].posY - loser[0].fontSize;

			DrawText(loser[0].text, (static_cast<int>(screenWidth) - MeasureText(loser[0].text, loser[0].fontSize)) / 2,
				loser[0].posY, loser[0].fontSize, YELLOW);

			DrawText(loser[1].text, (static_cast<int>(screenWidth) - MeasureText(loser[1].text, loser[1].fontSize)) / 2,
				loser[1].posY, loser[1].fontSize, YELLOW);

			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2, pressR.posY, pressR.fontSize, MAGENTA);

			DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, YELLOW);
		}

		static void drawWinningScreen()
		{
			winner[0].text = "You win";
			winner[1].text = "Congratulations!";
			pressR.text = "Press R to restart the game";
			pressEnter.text = "Press enter to go back to menu";

			winner[0].fontSize = static_cast<int>(55.0f * drawScaleY);
			winner[1].fontSize = static_cast<int>(40.0f * drawScaleY);
			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressR.fontSize = pressEnter.fontSize;

			winner[0].posY = static_cast<int>(screenHeight / 2.0f - winner[0].fontSize / 2.0f);
			winner[1].posY = static_cast<int>(winner[0].posY + winner[0].fontSize + winner[1].fontSize / 4.0f);
			pressEnter.posY = screenLimit.down - pressEnter.fontSize * 2;
			pressR.posY = winner[0].posY - winner[0].fontSize;

			DrawText(winner[0].text, (static_cast<int>(screenWidth) - MeasureText(winner[0].text, winner[0].fontSize)) / 2,
				winner[0].posY, winner[0].fontSize, YELLOW);

			DrawText(winner[1].text, (static_cast<int>(screenWidth) - MeasureText(winner[1].text, winner[1].fontSize)) / 2,
				winner[1].posY, winner[1].fontSize, YELLOW);

			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2, pressR.posY, pressR.fontSize, MAGENTA);

			DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, YELLOW);
		}

		//------------------------------------------------------------------------------

		void initialization()
		{
			restartGame();
			loadGameplayTextures();
			charactersInitialization();
		}

		void input()
		{
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				mouseDown = true;
			else if (mouseDown)
				mouseDown = false;

			if (IsKeyPressed(KEY_P)) 	
				pauseGame = !pauseGame;

			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_R))	
				restart = true;

			if (IsKeyPressed(KEY_M))	
				mute = !mute;

			#if DEBUG
			if (IsKeyPressed(KEY_L))
				lose = true;

			if (IsKeyPressed(KEY_W))
				win = true;
			#endif // DEBUG
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
				pauseGame = false;
				restartGame();
			}
			
			if (restart)
			{
				restart = false;
				restartGame();
				SetMusicVolume(gameplayStream, 0.1f);
			}

			if (!lose && !win)
			{
				if (!pauseGame)
				{
					gridUpdate();
					updateCharactersBars();

					if (characters::hitAnimationOn)
						characters::setHitAnimation();
				}
			}
			else if (lose)
			{
				SetMusicVolume(gameplayStream, 0.05f);

				if (!soundPLayed)
				{
					losingMatch = true;
					soundPLayed = true;
				}

				if (!IsSoundPlaying(losing) && !losingMatch && !sound2PLayed)
				{
					losingMatch2 = true;
					sound2PLayed = true;
				}				
			}
			else
			{
				SetMusicVolume(gameplayStream, 0.05f);

				if (!soundPLayed)
				{
					winningMatch = true;
					soundPLayed = true;
				}

				if (!IsSoundPlaying(winning) && !winningMatch && !sound2PLayed)
				{
					winningMatch2 = true;
					sound2PLayed = true;
				}
			}

			//audio
			if (!inGameplay)
			{
				inGameplay = true;
				playMusic();
			}
			if (!mute)
			{
				UpdateMusicStream(gameplayStream);
			}
			playSound();
			deactivateEvents();
		}

		void draw()
		{
			ClearBackground(WHITE); 

			if (!lose && !win)
			{
				if (!pauseGame)
				{
					DrawTextureEx(gameplayBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);

					gridDraw();
					drawCharacters();
				}
				else
				{
					DrawTextureEx(pauseBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);
					drawPauseScreen();
				}
			}
			else if (lose)
			{
				DrawTextureEx(pauseBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);
				drawLosingScreen();
			}
			else
			{
				DrawTextureEx(pauseBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);
				drawWinningScreen();
			}
		}

		void deinitialization()
		{
			
		}
	}
}

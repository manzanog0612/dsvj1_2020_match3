#include "scenes/rules.h"

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace rules
	{
		void initialization()
		{
			loadRulesTextures();
			const short fontSize = static_cast<int>(25.0f * drawScaleY);

			rulesText[firstLine].text = "The objetive of the game is to";
			rulesText[secondLine].text = "destroy all your enemies.";
			rulesText[thirdLine].text = "You can hurt them by selecting";
			rulesText[fourthLine].text = "3 or more planets, charging your";
			rulesText[fifthLine].text = "galaxy bar anf performing an attack";
			rulesText[sixthLine].text = "you can create power jewels by";
			rulesText[seventhLine].text = "combining 7 or more planets of";
			rulesText[eighthLine].text = "the same type.";

			pressEnter.text = "Press enter to go back to menu";

			rulesText[firstLine].posY = static_cast<int>(screenLimit.down / 3.5f);
			rulesText[secondLine].posY = static_cast<int>(rulesText[firstLine].posY + fontSize * 1.7f);
			rulesText[thirdLine].posY = static_cast<int>(rulesText[secondLine].posY + fontSize * 1.7f);
			rulesText[fourthLine].posY = static_cast<int>(rulesText[thirdLine].posY + fontSize * 1.7f);
			rulesText[fifthLine].posY = static_cast<int>(rulesText[fourthLine].posY + fontSize * 1.7f);
			rulesText[sixthLine].posY = static_cast<int>(rulesText[fifthLine].posY + fontSize * 1.7f);
			rulesText[seventhLine].posY = static_cast<int>(rulesText[sixthLine].posY + fontSize * 1.7f);
			rulesText[eighthLine].posY = static_cast<int>(rulesText[seventhLine].posY + fontSize * 1.7f);
		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
			}

			if (!mute)
				UpdateMusicStream(menuStream);
		}

		void draw()
		{
			const Color color = YELLOW;
			const short fontSize = static_cast<int>(25.0f * drawScaleY);
			float treasuresPosY = rulesText[fourthLine].posY + fontSize * 1.5f;

			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressEnter.posY = static_cast<int>(screenLimit.down - pressEnter.fontSize * 2);

			ClearBackground(BLACK);

			DrawTextureEx(menuBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(rulesBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);

			DrawText(rulesText[firstLine].text, (static_cast<int>(screenWidth)- MeasureText(rulesText[firstLine].text, fontSize))/ 2, rulesText[firstLine].posY, fontSize, color);
			DrawText(rulesText[secondLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[secondLine].text, fontSize)) / 2, rulesText[secondLine].posY, fontSize, color);
			DrawText(rulesText[thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[thirdLine].text, fontSize)) / 2, rulesText[thirdLine].posY, fontSize, color);
			DrawText(rulesText[fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[fourthLine].text, fontSize)) / 2, rulesText[fourthLine].posY, fontSize, color);
			DrawText(rulesText[fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[fifthLine].text, fontSize)) / 2, rulesText[fifthLine].posY, fontSize, color);
			DrawText(rulesText[sixthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[sixthLine].text, fontSize)) / 2, rulesText[sixthLine].posY, fontSize, color);
			DrawText(rulesText[seventhLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[seventhLine].text, fontSize)) / 2, rulesText[seventhLine].posY, fontSize, color);
			DrawText(rulesText[eighthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[eighthLine].text, fontSize)) / 2, rulesText[eighthLine].posY, fontSize, color);

			DrawText(pressEnter.text, (static_cast<int>(screenWidth) - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, YELLOW);
		}

		void deinitialization()
		{
			
		}
	}
}
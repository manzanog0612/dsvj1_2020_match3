#ifndef MENU_H
#define MENU_H 

namespace game
{
	namespace menu
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}
}

#endif // MENU_H
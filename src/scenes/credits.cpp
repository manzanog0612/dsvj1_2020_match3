#include "scenes/credits.h"

#include "raylib.h"
#include <iostream>

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace credits
	{
		void initialization()
		{
			loadCreditsTextures();

			const short fontSize = static_cast<int>(16 * drawScaleY);

			creditsText[firstLine].text = "Audio";
			creditsText[secondLine].text = "From the page freesound.org:";
			creditsText[thirdLine].text = "https://freesound.org";
			creditsText[fourthLine].text = "From youtube:";
			creditsText[fifthLine].text = "https://www.youtube.com";
			creditsText[sixthLine].text = "Textures";
			creditsText[seventhLine].text = "From hiclipart:";
			creditsText[eighthLine].text = "https://www.hiclipart.com";
			creditsText[ninethLine].text = "From hipwallpaper:";
			creditsText[tenthLine].text = "https://hipwallpaper.com";
			creditsText[eleventhLine].text = "All the rest of the assets were made by Guillermina Manzano"; 
			creditsText[twelfthLine].text = "Game desing and development";
			creditsText[thirteenthLine].text = "by Guillermina Manzano";

			pressEnter.text = "Press enter to go back to menu";

			for (short i = firstLine; i <= thirteenthLine; i++)
			{
				if (i == firstLine)
					creditsText[i].posY = static_cast<int>(screenHeight / 7.0f);
				else 
					creditsText[i].posY = static_cast<int>(creditsText[i - 1].posY + fontSize * 2.1f);

			}
		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
			}

			if (!mute)
				UpdateMusicStream(menuStream);
		}

		void draw()
		{
			ClearBackground(BLACK);

			DrawTextureEx(menuBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(creditsBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);

			const short fontSize = static_cast<int>(16.0f * drawScaleY);
			const Color color = BLACK;
			pressEnter.posY = static_cast<int>(screenLimit.down - pressEnter.fontSize * 2.0f);
			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);

			DrawText(creditsText[firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[firstLine].text, fontSize + 15)) / 2, creditsText[firstLine].posY, fontSize + 15, MAGENTA);
			DrawText(creditsText[secondLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[secondLine].text, fontSize + 4)) / 2, creditsText[secondLine].posY, fontSize + 4, YELLOW);
			DrawText(creditsText[thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirdLine].text, fontSize)) / 2, creditsText[thirdLine].posY, fontSize, color);
			DrawText(creditsText[fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fourthLine].text, fontSize + 4)) / 2, creditsText[fourthLine].posY, fontSize + 4, YELLOW);
			DrawText(creditsText[fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fifthLine].text, fontSize)) / 2, creditsText[fifthLine].posY, fontSize, color);
			DrawText(creditsText[sixthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[sixthLine].text, fontSize + 15)) / 2, creditsText[sixthLine].posY, fontSize + 15, MAGENTA);
			DrawText(creditsText[seventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[seventhLine].text, fontSize + 4)) / 2, creditsText[seventhLine].posY, fontSize + 4, YELLOW);
			DrawText(creditsText[eighthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eighthLine].text, fontSize)) / 2, creditsText[eighthLine].posY, fontSize, color);
			DrawText(creditsText[ninethLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[ninethLine].text, fontSize + 4)) / 2, creditsText[ninethLine].posY, fontSize + 4, YELLOW);
			DrawText(creditsText[tenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[tenthLine].text, fontSize)) / 2, creditsText[tenthLine].posY, fontSize, color);
			DrawText(creditsText[eleventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eleventhLine].text, fontSize)) / 2, creditsText[eleventhLine].posY, fontSize, color);
			DrawText(creditsText[twelfthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twelfthLine].text, fontSize + 15)) / 2, creditsText[twelfthLine].posY, fontSize + 15, MAGENTA);
			DrawText(creditsText[thirteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirteenthLine].text, fontSize)) / 2, creditsText[thirteenthLine].posY, fontSize, color);

			/*switch (actualScreen)
			{
			case 1:
				DrawText(creditsText[firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[firstLine].text, fontSize + 15)) / 2, creditsText[firstLine].posY, fontSize + 15, VIOLET);
				DrawText(creditsText[secondLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[secondLine].text, fontSize + 4)) / 2, creditsText[secondLine].posY, fontSize + 4, YELLOW);
				DrawText(creditsText[thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirdLine].text, fontSize)) / 2, creditsText[thirdLine].posY, fontSize, color);
				DrawText(creditsText[fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fourthLine].text, fontSize)) / 2, creditsText[fourthLine].posY, fontSize, color);
				DrawText(creditsText[fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fifthLine].text, fontSize)) / 2, creditsText[fifthLine].posY, fontSize, color);
				DrawText(creditsText[sixthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[sixthLine].text, fontSize)) / 2, creditsText[sixthLine].posY, fontSize, color);
				DrawText(creditsText[seventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[seventhLine].text, fontSize)) / 2, creditsText[seventhLine].posY, fontSize, color);
				DrawText(creditsText[eighthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eighthLine].text, fontSize)) / 2, creditsText[eighthLine].posY, fontSize, color);
				DrawText(creditsText[ninethLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[ninethLine].text, fontSize)) / 2, creditsText[ninethLine].posY, fontSize, color);
				DrawText(creditsText[tenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[tenthLine].text, fontSize)) / 2, creditsText[tenthLine].posY, fontSize, color);
				DrawText(creditsText[eleventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eleventhLine].text, fontSize)) / 2, creditsText[eleventhLine].posY, fontSize, color);
				DrawText(creditsText[twelfthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twelfthLine].text, fontSize)) / 2, creditsText[twelfthLine].posY, fontSize, color);
				DrawText(creditsText[thirteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirteenthLine].text, fontSize)) / 2, creditsText[thirteenthLine].posY, fontSize, color);
				DrawText(creditsText[fourteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fourteenthLine].text, fontSize)) / 2, creditsText[fourteenthLine].posY, fontSize, color);
				DrawText(creditsText[fifteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fifteenthLine].text, fontSize)) / 2, creditsText[fifteenthLine].posY, fontSize, color);
				DrawText(creditsText[sixteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[sixteenthLine].text, fontSize)) / 2, creditsText[sixteenthLine].posY, fontSize, color);
				DrawText(creditsText[seventeenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[seventeenthLine].text, fontSize)) / 2, creditsText[seventeenthLine].posY, fontSize, color);
				DrawText(creditsText[eighteenth].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eighteenth].text, fontSize)) / 2, creditsText[eighteenth].posY, fontSize, color);
				break;
			case 2:
				DrawText(creditsText[firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[firstLine].text, fontSize + 15)) / 2, creditsText[firstLine].posY, fontSize + 15, VIOLET);
				DrawText(creditsText[nineteenth].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[nineteenth].text, fontSize + 4)) / 2, creditsText[nineteenth].posY, fontSize + 4, YELLOW);
				DrawText(creditsText[twentieth].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twentieth].text, fontSize)) / 2, creditsText[twentieth].posY, fontSize, color);
				DrawText(creditsText[twenty_firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_firstLine].text, fontSize)) / 2, creditsText[twenty_firstLine].posY, fontSize, color);
				DrawText(creditsText[twenty_secondLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_secondLine].text, fontSize)) / 2, creditsText[twenty_secondLine].posY, fontSize, color);
				DrawText(creditsText[twenty_thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_thirdLine].text, fontSize)) / 2, creditsText[twenty_thirdLine].posY, fontSize, color);
				break;
			case 3:
				DrawText(creditsText[twenty_fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_fourthLine].text, fontSize + 15)) / 2, creditsText[twenty_fourthLine].posY, fontSize + 15, VIOLET);
				DrawText(creditsText[twenty_fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_fifthLine].text, fontSize + 4)) / 2, creditsText[twenty_fifthLine].posY, fontSize + 4, YELLOW);
				DrawText(creditsText[twenty_sixthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_sixthLine].text, fontSize)) / 2, creditsText[twenty_sixthLine].posY, fontSize, color);
				DrawText(creditsText[twenty_seventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_seventhLine].text, fontSize)) / 2, creditsText[twenty_seventhLine].posY, fontSize, color);
				DrawText(creditsText[twenty_eighthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_eighthLine].text, fontSize)) / 2, creditsText[twenty_eighthLine].posY, fontSize, color);
				DrawText(creditsText[twenty_ninethLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twenty_ninethLine].text, fontSize)) / 2, creditsText[twenty_ninethLine].posY, fontSize, color);
				DrawText(creditsText[thirtiethLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirtiethLine].text, fontSize)) / 2, creditsText[thirtiethLine].posY, fontSize, color);
				break;
			case 4:
				DrawText(creditsText[thirty_firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirty_firstLine].text, fontSize + 15)) / 2, creditsText[thirty_firstLine].posY, fontSize + 15, VIOLET);
				DrawText(creditsText[thirty_secondLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirty_secondLine].text, fontSize + 7)) / 2, creditsText[thirty_secondLine].posY, fontSize + 7, color);
				break;
			default:
				break;
			}*/

			DrawText(pressEnter.text, (static_cast<int>(screenWidth) - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, VIOLET);
		}

		void deinitialization()
		{

		}
	}
}
#include "characters.h"

#include <stdlib.h>

#include "game_vars/global_vars.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"
#include "jewels.h"

using namespace game;
using namespace global_vars;
using namespace textures;
using namespace events;
using namespace jewels;

namespace game
{
	namespace characters
	{
		Characters enemies[typesOfEnemies];
		Characters player;
		Bars playerBars;
		Bars enemieBars;
		short enemieOnScreen;
		double timeForNextHit;

		bool hitAnimationOn = false;
		bool hitToPlayer = false;
		bool hitToEnemie = false;

		short framesCounter;
		short currentFrame;
		Rectangle frameRec;

		void charactersInitialization()
		{
			for (short i = 0; i < typesOfEnemies; i++)
			{
				enemies[i].dimentions.height = enemiesT[i].height;
				enemies[i].dimentions.width = enemiesT[i].width;
				enemies[i].dimentions.x = (gameSpace.x + gameSpace.width) - enemies[i].dimentions.width;
				enemies[i].dimentions.y = (gameSpace.y * 11.0f / 12.0f ) - enemies[i].dimentions.height;
			}

			player.dimentions.height = playerT.height;
			player.dimentions.width = playerT.width;
			player.dimentions.x = gameSpace.x;
			player.dimentions.y = screenHeight / 20.0f;

			const float differenceBetweenBarsInY = player.dimentions.height * 1.39f - player.dimentions.height * 1.255f;
			const float barTotalWidth = charactersBarsT.width / 1.2f;
			// Bars initialization

			playerBars.attack = 0;
			playerBars.live = maxBarsPoint;
			playerBars.power = 0;
			playerBars.shield = maxBarsPoint;
			playerBars.liveR.x = gameSpace.x + charactersBarsT.width / 7.8f;
			playerBars.liveR.y = player.dimentions.y + player.dimentions.height * 1.255f;
			playerBars.powerR.x = playerBars.liveR.x;
			playerBars.powerR.y = playerBars.liveR.y + differenceBetweenBarsInY;
			playerBars.attackR.x = playerBars.liveR.x;
			playerBars.attackR.y = playerBars.powerR.y + differenceBetweenBarsInY;
			playerBars.shieldR.x = playerBars.liveR.x;
			playerBars.shieldR.y = playerBars.attackR.y + differenceBetweenBarsInY;

			playerBars.liveR.height = charactersBarsT.height / 9.0f;
			playerBars.liveR.width = barTotalWidth;
			playerBars.powerR.height = playerBars.liveR.height;
			playerBars.powerR.width = 0;
			playerBars.attackR.height = playerBars.liveR.height;
			playerBars.attackR.width = 0;
			playerBars.shieldR.height = playerBars.liveR.height;
			playerBars.shieldR.width = barTotalWidth;

			enemieBars.attack = 0;
			enemieBars.live = maxBarsPoint;
			enemieBars.power = 0;
			enemieBars.shield = maxBarsPoint;
			enemieBars.liveR.x = gameSpace.x + gameSpace.width - charactersBarsT.width + charactersBarsT.width / 7.8f;
			enemieBars.liveR.y = player.dimentions.y + player.dimentions.height * 0.045f;
			enemieBars.powerR.x = enemieBars.liveR.x;
			enemieBars.powerR.y = enemieBars.liveR.y + differenceBetweenBarsInY;
			enemieBars.attackR.x = enemieBars.liveR.x;
			enemieBars.attackR.y = enemieBars.powerR.y + differenceBetweenBarsInY;
			enemieBars.shieldR.x = enemieBars.liveR.x;
			enemieBars.shieldR.y = enemieBars.attackR.y + differenceBetweenBarsInY;

			enemieBars.liveR.height = charactersBarsT.height / 9.0f;
			enemieBars.liveR.width = barTotalWidth;
			enemieBars.powerR.height = playerBars.liveR.height;
			enemieBars.powerR.width = 0;
			enemieBars.attackR.height = playerBars.liveR.height;
			enemieBars.attackR.width = 0;
			enemieBars.shieldR.height = playerBars.liveR.height;
			enemieBars.shieldR.width = barTotalWidth;

			timeForNextHit = GetTime() + 5;
			enemieOnScreen = 0;
		}

		static void setSoundEnemieHurt()
		{
			monsterHurt = true;
		}

		static void setSoundPlayerHurt()
		{
			playerHurt = true;
		}

		static bool chechTimeToUpdateEnemiesBars()
		{
			return GetTime() >= timeForNextHit;
		}

		static void setEnemiesDamage(short playerDamage)
		{
			if (enemieBars.shield > 0)
				enemieBars.shield -= playerDamage;
			else
				enemieBars.live -= playerDamage;
		}

		static void setPlayersDamage(short enemieDamage)
		{
			if (playerBars.shield > 0)
				playerBars.shield -= enemieDamage;
			else
				playerBars.live -= enemieDamage;
		}

		static void setEnemyAttack()
		{
			short aux = rand() % 2;
			timeForNextHit = GetTime() + 3;

			if (aux == 1)
				enemieBars.power += 5;
			else
				enemieBars.attack += 10;

			if (enemieBars.attack == maxBarsPoint)
			{
				enemieBars.attack = 0;
				setPlayersDamage((enemieOnScreen + 1) * 4);
				setSoundPlayerHurt();
				hitToPlayer = true;
				hitAnimationOn = true;
			}
			if (enemieBars.power == maxBarsPoint)
			{
				enemieBars.power = 0;
				setPlayersDamage((enemieOnScreen + 1) * 6);
				setSoundPlayerHurt();
				hitToPlayer = true;
				hitAnimationOn = true;
			}
		}

		static bool checkEnemyAlive()
		{
			return enemieBars.live > 0;
		}

		void setHitAnimation()
		{
			const short spritesPerRow = 6;
			const short spritesPerColumn = 7;
			const short amountOfSprites = spritesPerColumn * spritesPerRow;

			framesCounter++;

			if (framesCounter >= (fps / amountOfSprites))
			{
				framesCounter = 0;
				currentFrame++;

				if (currentFrame > amountOfSprites - 1)
				{
					hitAnimationOn = false;
					hitToEnemie = false;
					hitToPlayer = false;
					currentFrame = -1;
				}

				if (currentFrame < spritesPerRow)
				{
					frameRec.x = static_cast<float>(currentFrame) * static_cast<float>(hit.width / spritesPerRow);
					frameRec.y = 0;
				}
				else if (currentFrame < spritesPerRow * 2)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow) * static_cast<float>(hit.width / spritesPerRow);
					frameRec.y = static_cast<float>(hit.height / spritesPerColumn);
				}
				else if (currentFrame < spritesPerRow * 3)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow * 2) * static_cast<float>(hit.width / spritesPerRow);
					frameRec.y = static_cast<float>(hit.height * 2 / spritesPerColumn);
				}
				else if (currentFrame < spritesPerRow * 4)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow * 3) * static_cast<float>(hit.width / spritesPerRow);
					frameRec.y = static_cast<float>(hit.height * 3 / spritesPerColumn);
				}
				else if (currentFrame < spritesPerRow * 5)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow * 4) * static_cast<float>(hit.width / spritesPerRow);
					frameRec.y = static_cast<float>(hit.height * 4 / spritesPerColumn);
				}
				else if (currentFrame < spritesPerRow * 6)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow * 5) * static_cast<float>(hit.width / spritesPerRow);
					frameRec.y = static_cast<float>(hit.height * 5 / spritesPerColumn);
				}
				else if (currentFrame < spritesPerRow * 7)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow * 6) * static_cast<float>(hit.width / spritesPerRow);
					frameRec.y = static_cast<float>(hit.height * 6 / spritesPerColumn);
				}

				frameRec.width = hit.width / spritesPerRow;
				frameRec.height = hit.height / spritesPerColumn;
			}
		}

		void updateCharactersBars()
		{
			const float barTotalWidth = charactersBarsT.width / 1.2f;
			const short playerAttack = 10;
			const short playerPower = 20;

			if (playerBars.attack == maxBarsPoint)
			{
				playerBars.attack = 0;
				setSoundEnemieHurt();
				setEnemiesDamage(playerAttack);
				hitToEnemie = true;
				hitAnimationOn = true;
			}

			if (playerBars.power == maxBarsPoint)
			{
				playerBars.power = 0;
				setSoundEnemieHurt();
				setEnemiesDamage(playerPower);
				hitToEnemie = true;
				hitAnimationOn = true;
			}

			if (chechTimeToUpdateEnemiesBars())
			{
				setEnemyAttack();
			}

			playerBars.attackR.width = barTotalWidth * playerBars.attack / maxBarsPoint;
			playerBars.liveR.width = barTotalWidth * playerBars.live / maxBarsPoint;
			playerBars.powerR.width = barTotalWidth * playerBars.power / maxBarsPoint;
			playerBars.shieldR.width = barTotalWidth * playerBars.shield / maxBarsPoint;

			enemieBars.attackR.width = barTotalWidth * enemieBars.attack / maxBarsPoint;
			enemieBars.liveR.width = barTotalWidth * enemieBars.live / maxBarsPoint;
			enemieBars.powerR.width = barTotalWidth * enemieBars.power / maxBarsPoint;
			enemieBars.shieldR.width = barTotalWidth * enemieBars.shield / maxBarsPoint;

			if (!checkEnemyAlive())
			{
				enemieOnScreen++;
				enemieBars.live = maxBarsPoint;
				enemieBars.attack = 0;
				enemieBars.power = 0;
				enemieBars.shield = maxBarsPoint;
			}
			
			if (enemieOnScreen == typesOfEnemies)
				win = true;

			if (playerBars.live <= 0)
				lose = true;
		}

		void drawCharacters()
		{
			DrawTextureEx(enemiesT[enemieOnScreen].texture, { enemies[enemieOnScreen].dimentions.x, enemies[enemieOnScreen].dimentions.y }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(playerT.texture, { player.dimentions.x, player.dimentions.y }, 0.0f, 1.0f, WHITE);

			DrawRectangleRec(playerBars.liveR, RED);
			DrawRectangleRec(playerBars.attackR, GREEN);
			DrawRectangleRec(playerBars.powerR, YELLOW);
			DrawRectangleRec(playerBars.shieldR, BLUE);
			DrawRectangleRec(enemieBars.liveR, RED);
			DrawRectangleRec(enemieBars.attackR, GREEN);
			DrawRectangleRec(enemieBars.powerR, YELLOW);
			DrawRectangleRec(enemieBars.shieldR, BLUE);

			DrawTextureEx(charactersBarsT.texture, { gameSpace.x,  player.dimentions.y + player.dimentions.height * 1.2f }, 0.0f, 1.0f, WHITE);
			DrawTextureEx(charactersBarsT.texture, { gameSpace.x + gameSpace.width - charactersBarsT.width, player.dimentions.y }, 0.0f, 1.0f, WHITE);

			if (hitToPlayer && hitAnimationOn)
				DrawTextureRec(hit.texture, frameRec, { player.dimentions.x * 1.4f , player.dimentions.y * 1.4f }, WHITE);
			else if (hitToEnemie && hitAnimationOn)
				DrawTextureRec(hit.texture, frameRec, { enemies[enemieOnScreen].dimentions.x * 1.25f, enemies[enemieOnScreen].dimentions.y * 1.25f }, WHITE);
		}
	}
}
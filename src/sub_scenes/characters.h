#ifndef CHARACTERS_H
#define CHARACTERS_H

#include "raylib.h"

namespace game
{
	namespace characters
	{
		struct Characters
		{
			Rectangle dimentions;
		};

		struct Bars
		{
			short live;
			short power;
			short shield;
			short attack;
			Rectangle liveR;
			Rectangle powerR;
			Rectangle shieldR;
			Rectangle attackR;
		};

		extern Bars playerBars;
		extern bool hitAnimationOn;

		void charactersInitialization();

		void updateCharactersBars();

		void setHitAnimation();

		void drawCharacters();
	}
}

#endif // CHARACTERS_H

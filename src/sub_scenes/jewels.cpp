#include "jewels.h"

#include <cmath>
#include <vector>

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "characters.h"
#include "resourses/events.h"
#include "resourses/audio.h"
#include "resourses/textures.h"

using namespace global_vars;
using namespace global_drawing_vars;
using namespace events;
using namespace audio;
using namespace textures;

namespace game
{
	namespace jewels
	{
		Jewels board[jewelsRows][jewelsColumns];

		std::vector<Jewels> selectedJewels;
		std::vector<Activator> activator;
		PLANETS laterPlanetType;
		bool jewelsUnhighlighted = true;
		short selectionSize = 0;
		bool movingJewels = false;
		bool movementSet = false;

		float spaceBetweenJewelsX = 6.0f * drawScaleX;
		float spaceBetweenJewelsY = 6.0f * drawScaleY;

		short framesCounter;
		short currentFrame;
		Rectangle frameRec;

		bool jewelEliminationOn;

		void gridInitialization()
		{
			jewelEliminationOn = false;
			currentFrame = -1;

			gameSpace.x = 30.0f * drawScaleX;
			gameSpace.y = screenHeight / 2.1f;
			gameSpace.width = screenWidth - gameSpace.x * 2;
			gameSpace.height = screenHeight - gameSpace.y - 30.f * drawScaleY;

			for (short i = 0; i < jewelsRows; i++)
			{
				for (short j = 0; j < jewelsColumns; j++)
				{
					board[i][j].active = true;

					if (board[i][j].selected != false)
						board[i][j].selected = false;

					board[i][j].dimentions.width = (gameSpace.width / jewelsColumns) - (spaceBetweenJewelsX * (jewelsColumns - 1) / jewelsColumns);
					board[i][j].dimentions.height = board[i][j].dimentions.width;

					board[i][j].dimentions.x = gameSpace.x + j * (board[i][j].dimentions.width + spaceBetweenJewelsX);
					board[i][j].dimentions.y = gameSpace.y + i * (board[i][j].dimentions.height + spaceBetweenJewelsY);

					board[i][j].type = static_cast<PLANETS>(rand() % 6);

					board[i][j].row = i;
					board[i][j].column = j;
				}
			}
		}

		static void setSoundJewelSelected()
		{
			jewelIsSelected = true;
		}

		static void setSoundJewelsEliminated()
		{
			jewelsEliminated = true;
		}

		static short getIndexOfJewelToUnhighlight()
		{
			for (unsigned short k = 0; k < selectedJewels.size(); k++)
			{
				if (mouse.posX >= selectedJewels[k].dimentions.x && mouse.posX <= selectedJewels[k].dimentions.x + selectedJewels[k].dimentions.width &&
					mouse.posY >= selectedJewels[k].dimentions.y && mouse.posY <= selectedJewels[k].dimentions.y + selectedJewels[k].dimentions.height)
				{
					return k;
				}
			}

			return static_cast<short>(selectedJewels.size() - 1);
		}

		static bool checkIfJewelIsProximate(jewels::Jewels jewel1, jewels::Jewels jewel2)
		{
			for (short i = jewel1.row - 1; i <= jewel1.row + 1; i++)
			{
				for (short j = jewel1.column - 1; j <= jewel1.column + 1; j++)
				{
					if (jewel2.row == i && jewel2.column == j)
						return true;
				}
			}

			return false;
		}

		static void highlightJewelTouched()
		{
			mouse.posX = GetMouseX();
			mouse.posY = GetMouseY();

			for (short i = 0; i < jewelsRows; i++)
			{
				for (short j = 0; j < jewelsColumns; j++)
				{
					if (selectedJewels.size() == 0)
					{
						if (mouse.posX >= board[i][j].dimentions.x && mouse.posX <= board[i][j].dimentions.x + board[i][j].dimentions.width &&
							mouse.posY >= board[i][j].dimentions.y && mouse.posY <= board[i][j].dimentions.y + board[i][j].dimentions.height &&
							!board[i][j].selected)
						{
							board[i][j].selected = true;
							selectedJewels.push_back(board[i][j]);
							laterPlanetType = board[i][j].type;

							setSoundJewelSelected();
						}
					}
					else if (checkIfJewelIsProximate(selectedJewels[selectedJewels.size() - 1], board[i][j]))
					{
						if (mouse.posX >= board[i][j].dimentions.x && mouse.posX <= board[i][j].dimentions.x + board[i][j].dimentions.width &&
							mouse.posY >= board[i][j].dimentions.y && mouse.posY <= board[i][j].dimentions.y + board[i][j].dimentions.height &&
							!board[i][j].selected && board[i][j].type == laterPlanetType)
						{
							board[i][j].selected = true;
							selectedJewels.push_back(board[i][j]);

							setSoundJewelSelected();
						}

						if (getIndexOfJewelToUnhighlight() < static_cast<short>(selectedJewels.size() - 1))
						{
							selectionSize = static_cast<short>(selectedJewels.size());

							for (short k = selectionSize - 1; k > getIndexOfJewelToUnhighlight(); k--)
							{
								if (board[selectedJewels[k].row][selectedJewels[k].column].selected)
									board[selectedJewels[k].row][selectedJewels[k].column].selected = false;

								selectedJewels.pop_back();
							}

							setSoundJewelSelected();
						}
					}
				}
			}
		}

		static void unhightlightSlectedJewels()
		{
			const short minimumEliminationAmount = 3;

			if (selectedJewels.size() < minimumEliminationAmount)
				selectedJewels.clear();
			else
			{
				selectionSize = static_cast<short>(selectedJewels.size());

				for (short k = selectionSize - 1; k >= 0; k--)
				{
					for (short i = 0; i < jewelsRows; i++)
					{
						for (short j = 0; j < jewelsColumns; j++)
						{
							if (selectedJewels[k].dimentions.x == board[i][j].dimentions.x &&
								selectedJewels[k].dimentions.y == board[i][j].dimentions.y)
							{
								board[i][j].active = false;
							}
						}
					}
				}

				for (short  i = 0; i < selectionSize; i++)
				{
					switch (selectedJewels[i].type)
					{
					case game::elements_properties::PLANETS::BLUE_P:
						if (characters::playerBars.shield < maxBarsPoint)
							characters::playerBars.shield++;
						else if (characters::playerBars.live < maxBarsPoint)
							characters::playerBars.live++;
						break;
					case game::elements_properties::PLANETS::RED_P:
						if (characters::playerBars.shield < maxBarsPoint)
							characters::playerBars.shield++;
						else if (characters::playerBars.live < maxBarsPoint)
							characters::playerBars.live++;
						break;
					case game::elements_properties::PLANETS::YELLOW_P:
						if (characters::playerBars.power + 2 > maxBarsPoint)
							characters::playerBars.power = maxBarsPoint;
						else if (characters::playerBars.attack < maxBarsPoint)
							characters::playerBars.power += 2;
						break;
					case game::elements_properties::PLANETS::GREEN_P:
						if (characters::playerBars.power + 5 > maxBarsPoint)
							characters::playerBars.power = maxBarsPoint;
						if (characters::playerBars.attack < maxBarsPoint)
							characters::playerBars.attack += 5;
						break;
					case game::elements_properties::PLANETS::ORANGE_P:
						if (characters::playerBars.power + 2 > maxBarsPoint)
							characters::playerBars.power = maxBarsPoint;
						else if (characters::playerBars.attack < maxBarsPoint)
							characters::playerBars.power += 2;
						break;
					case game::elements_properties::PLANETS::VIOLET_P:
						if (characters::playerBars.power + 5 > maxBarsPoint)
							characters::playerBars.power = maxBarsPoint;
						if (characters::playerBars.attack < maxBarsPoint)
							characters::playerBars.attack += 5;
						break;
					case game::elements_properties::PLANETS::BLACKHOLE:
						break;
					case game::elements_properties::PLANETS::FANTASYPLANET:
						break;
					default:
						break;
					}
				}

				selectedJewels.clear();

				jewelEliminationOn = true;
			}

			for (short i = 0; i < jewelsRows; i++)
			{
				for (short j = 0; j < jewelsColumns; j++)
				{
					if (board[i][j].selected)
						board[i][j].selected = false;
				}
			}
		}

		static bool checkIfAnyJewelDeactivated(short column)
		{
			for (short i = jewelsRows - 1; i >= 0; i++)
			{
				if (!board[i][column].active)
					return true;
			}

			return false;
		}

		static void setJewelsForMovement()
		{
			short auxDif;
			std::vector<short> deactivatedJewelRows;
			Activator auxActivator;
			short i = 0;

			for (short j = jewelsColumns - 1; j >= 0; j--)
			{
				if (checkIfAnyJewelDeactivated(j))
				{
					for (short k = jewelsRows - 1; k > 0; k--)
					{
						if (!board[k][j].active && board[k][j].row > i)
							i = k;
					}

					if (!movingJewels)
						movingJewels = true;

					for (short k = i; k >= 0; k--)
					{
						if (!board[k][j].active)
						{
							deactivatedJewelRows.push_back(k);
						}
					}

					auxDif = 0;

					for (short k = i; k >= 0; k--)
					{
						for (short ii = 0; ii < static_cast<short>(deactivatedJewelRows.size()); ii++)
						{
							if (k == deactivatedJewelRows[ii])
								auxDif++;
						}

						if (board[k][j].active)
						{
							board[k][j].row += auxDif;
						}
					}

					auxActivator.amountOfJewels = static_cast<short>(deactivatedJewelRows.size());
					auxActivator.row = auxDif - 1;
					auxActivator.column = j;

					activator.push_back(auxActivator);

					deactivatedJewelRows.clear();

					if (!movementSet)
						movementSet = true;
					
				}
			}
		}

		static void reactivateJewels()
		{
			for (short i = 0; i < static_cast<short>(activator.size()); i++)
			{
				for (short j = 0; j < activator[i].amountOfJewels; j++)
				{
					for (short k = jewelsRows - 1; k >= 0; k--)
					{
						if (!board[k][activator[i].column].active)
						{
							board[k][activator[i].column].active = true;
							board[k][activator[i].column].row = activator[i].row - j;
							board[k][activator[i].column].type = static_cast<PLANETS>(rand() % 6);
							board[k][activator[i].column].dimentions.y = gameSpace.y + board[k][activator[i].column].row * 
								(board[k][activator[i].column].dimentions.height + spaceBetweenJewelsY);
							break;
						}
					}
				}
			}

			activator.clear();
		}

		static void changeGridIndexValues()
		{
			Jewels aux;

			for (short i = 0; i < jewelsRows; i++)
			{
				for (short j = 0; j < jewelsColumns; j++)
				{
					if (i != board[i][j].row)
					{
						for (short k = 0; k < jewelsRows; k++)
						{
							if (board[k][j].row == i)
							{
								aux = board[i][j];
								board[i][j] = board[k][j];
								board[k][j] = aux;
								break;
							}
						}
					}
				}
			}
		}

		static bool checkNewMatches()
		{
			const short minimumEliminationAmount = 3;
			PLANETS aux;
			short auxAmount = 1;

			for (short i = jewelsRows - 1; i >= 0; i--)
			{
				for (short j = 0; j < jewelsColumns; j++)
				{
					if (j == 0)
					{
						aux = board[i][j].type;
					}
					else if (aux == board[i][j].type)
					{
						auxAmount++;
					}
					else
					{
						if (auxAmount >= minimumEliminationAmount)
							return true;

						auxAmount = 1;
						selectedJewels.clear();
						aux = board[i][j].type;
					}

					selectedJewels.push_back(board[i][j]);
				}
			}

			if (auxAmount >= minimumEliminationAmount)
				return true;

			auxAmount = 1;

			for (short j = 0; j < jewelsColumns; j++)
			{
				for (short i = jewelsRows - 1; i >= 0; i--)
				{
					if (i == jewelsRows - 1)
						aux = board[i][j].type;
					else if (aux == board[i][j].type)
					{
						auxAmount++;
					}
					else
					{
						if (auxAmount >= minimumEliminationAmount)
							return true;

						auxAmount = 1;
						selectedJewels.clear();
						aux = board[i][j].type;
					}

					selectedJewels.push_back(board[i][j]);
				}
			}

			if (auxAmount >= minimumEliminationAmount)
				return true;
			else
			{
				selectedJewels.clear();
				return false;
			}
		}

		static void moveJewels()
		{
			float auxPosY;
			short jewelsInFinalPosition = 0;
			float speed = 100.0f * GetFrameTime();

			for (short i = jewelsRows - 1; i >= 0; i--)
			{
				for (short j = jewelsColumns - 1; j >= 0; j--)
				{
					auxPosY = gameSpace.y + board[i][j].row * (board[i][j].dimentions.height + spaceBetweenJewelsY);

					if (auxPosY != board[i][j].dimentions.y)
					{
						if (board[i][j].dimentions.y + speed > auxPosY)
						{
							board[i][j].dimentions.y = auxPosY;
							jewelsInFinalPosition++;
						}
						else
							board[i][j].dimentions.y += speed;
					}
					else
					{
						jewelsInFinalPosition++;
					}
				}
			}


			if (jewelsInFinalPosition == jewelsRows * jewelsColumns)
			{
				movingJewels = false;
				movementSet = false;
				reactivateJewels();
				changeGridIndexValues();
			}
		}

		static void jewelsEliminationAnimation()
		{
			const short spritesPerRow = 5;
			const short spritesPerColumn = 4;
			const short amountOfSprites = spritesPerColumn * spritesPerRow;

			framesCounter++;

			if (framesCounter >= (fps / amountOfSprites))
			{
				framesCounter = 0;
				currentFrame++;

				if (currentFrame > amountOfSprites - 1) 
				{
					movingJewels = true; 
					jewelEliminationOn = false;
					currentFrame = -1; 
				}

				if (currentFrame < spritesPerRow)
				{
					frameRec.x = static_cast<float>(currentFrame) * static_cast<float>(jewelsExplotion.width / spritesPerRow);
					frameRec.y = 0;
				}
				else if (currentFrame < spritesPerRow * 2)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow) * static_cast<float>(jewelsExplotion.width / spritesPerRow);
					frameRec.y = static_cast<float>(jewelsExplotion.height / spritesPerColumn);
				}
				else if (currentFrame < spritesPerRow * 3)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow * 2) * static_cast<float>(jewelsExplotion.width / spritesPerRow);
					frameRec.y = static_cast<float>(jewelsExplotion.height * 2 / spritesPerColumn);
				}
				else if (currentFrame < spritesPerRow * 4)
				{
					frameRec.x = static_cast<float>(currentFrame - spritesPerRow * 3) * static_cast<float>(jewelsExplotion.width / spritesPerRow);
					frameRec.y = static_cast<float>(jewelsExplotion.height * 3 / spritesPerColumn);
				}

				frameRec.width = jewelsExplotion.width / spritesPerRow;
				frameRec.height = jewelsExplotion.height / spritesPerColumn;
			}
		}
		
		void gridUpdate()
		{
			if (mouseDown && !movingJewels)
			{
				highlightJewelTouched();

				if (jewelsUnhighlighted)
					jewelsUnhighlighted = false;
			}
			else if ((!jewelsUnhighlighted || checkNewMatches()) && !movingJewels)
			{
				unhightlightSlectedJewels();
				jewelsUnhighlighted = true;

				if (!IsSoundPlaying(jewelsExplosion))
					setSoundJewelsEliminated();
			}

			if (jewelEliminationOn)
				jewelsEliminationAnimation();

			if (!movementSet)
				setJewelsForMovement();

			if (movingJewels)
				moveJewels();
		}

		void gridDraw()
		{
			Color color;

			for (short i = 0; i < jewelsRows; i++)
			{
				for (short j = 0; j < jewelsColumns; j++)
				{
					switch (board[i][j].type)
					{
					case PLANETS::BLUE_P:	color = BLUE; break;
					case PLANETS::RED_P:	color = RED; break;
					case PLANETS::YELLOW_P: color = YELLOW; break;
					case PLANETS::GREEN_P:	color = GREEN; break;
					case PLANETS::ORANGE_P:	color = ORANGE; break;
					case PLANETS::VIOLET_P: color = PURPLE; break;
					default:break;
					}

					if (board[i][j].active)
					{
						#if DEBUG
						DrawRectangleLinesEx(board[i][j].dimentions, 1, color);
						#endif // DEBUG

						DrawTextureEx(planetsT[static_cast<int>(board[i][j].type)].texture, { board[i][j].dimentions.x, board[i][j].dimentions.y }, 0.0f, 1.0f, WHITE);

						if (board[i][j].selected)
							DrawRectangleLinesEx(board[i][j].dimentions, 3, MAGENTA);
					}
					else if (jewelEliminationOn)
					{
						DrawTextureRec(jewelsExplotion.texture, frameRec, { board[i][j].dimentions.x, board[i][j].dimentions.y }, WHITE);
					}
				}
			}
		}
	}
}
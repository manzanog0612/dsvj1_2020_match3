#ifndef JEWELS_H
#define JEWELS_H

#include "raylib.h"

#include "elements/elements_properties.h"

using namespace game;
using namespace elements_properties;

namespace game
{
	namespace jewels
	{
		struct Jewels
		{
			PLANETS type;
			bool active;
			bool selected;
			short row;
			short column;
			Rectangle dimentions;
		};

		struct Activator
		{
			short row;
			short column;
			short amountOfJewels;
		};

		const short jewelsRows = 7;
		const short jewelsColumns = 9;

		extern jewels::Jewels board[jewelsRows][jewelsColumns];

		void gridInitialization();

		void gridUpdate();

		void gridDraw();
	}
}

#endif // JEWELS_H

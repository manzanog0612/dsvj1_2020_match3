#include "resourses/textures.h"

#include "raylib.h"

#include "configurations/configurations.h"
#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "sub_scenes/jewels.h"

using namespace configurations;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace jewels;

namespace game
{
	namespace textures
	{
		Textures menuBackground;
		Textures gameplayBackground;
		Textures rulesBackground;
		Textures planetsT[typesOfPlanets];
		Textures enemiesT[typesOfEnemies];
		Textures playerT;
		Textures charactersBarsT;
		Textures jewelsExplotion;
		Textures pauseBackground;
		Textures arrowNext;
		Textures arrowPrevious;
		Textures creditsBackground;
		Textures hit;

		void loadMenuTextures()
		{
			//images
			Image menuBackgroundAux = LoadImage("res/assets/textures/matchMenuBackground.png");

			//images to images with pointer to resize
			Image* menuBackgroundAuxP = &menuBackgroundAux;

			//width

			//height

			//resizing
			ImageResize(menuBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));

			//transformation from image* to image 
			menuBackgroundAux = *menuBackgroundAuxP;

			//transformation from image to texture2D
			menuBackground.texture = LoadTextureFromImage(menuBackgroundAux);
		}

		void loadGameplayTextures()
		{
			//images
			Image gameplayBackgroundAux = LoadImage("res/assets/textures/matchGameplayBackground.png");
			Image playerTAux = LoadImage("res/assets/textures/player.png");
			Image charactersBarsTAux = LoadImage("res/assets/textures/charactersBars.png");
			Image jewelsExplotionAux = LoadImage("res/assets/textures/jewelExplotion.png");
			Image pauseBackgroundAux = LoadImage("res/assets/textures/pauseBackground.png");
			Image hitAux = LoadImage("res/assets/textures/hit.png");
			Image planetsTAux[typesOfPlanets];
			Image enemiesTAux[typesOfEnemies];
			
			
			planetsTAux[static_cast<int>(PLANETS::BLUE_P)] = LoadImage("res/assets/textures/bluePlanet.png");
			planetsTAux[static_cast<int>(PLANETS::RED_P)] = LoadImage("res/assets/textures/redPlanet.png");
			planetsTAux[static_cast<int>(PLANETS::YELLOW_P)] = LoadImage("res/assets/textures/yellowPlanet.png");
			planetsTAux[static_cast<int>(PLANETS::GREEN_P)] = LoadImage("res/assets/textures/greenPlanet.png");
			planetsTAux[static_cast<int>(PLANETS::ORANGE_P)] = LoadImage("res/assets/textures/orangePlanet.png");
			planetsTAux[static_cast<int>(PLANETS::VIOLET_P)] = LoadImage("res/assets/textures/violetPlanet.png");
			planetsTAux[static_cast<int>(PLANETS::BLACKHOLE)] = LoadImage("res/assets/textures/blackHole.png");
			planetsTAux[static_cast<int>(PLANETS::FANTASYPLANET)] = LoadImage("res/assets/textures/fantasyPlanet.png");
			enemiesTAux[static_cast<int>(ENEMIES::MONSTER1)] = LoadImage("res/assets/textures/monster1.png");
			enemiesTAux[static_cast<int>(ENEMIES::MONSTER2)] = LoadImage("res/assets/textures/monster2.png");
			enemiesTAux[static_cast<int>(ENEMIES::MONSTER3)] = LoadImage("res/assets/textures/monster3.png");
			enemiesTAux[static_cast<int>(ENEMIES::MONSTER4)] = LoadImage("res/assets/textures/monster4.png");
			enemiesTAux[static_cast<int>(ENEMIES::MONSTER5)] = LoadImage("res/assets/textures/monster5.png");
			enemiesTAux[static_cast<int>(ENEMIES::MONSTER6)] = LoadImage("res/assets/textures/monster6.png");

			//images to images with pointer to resize
			Image* gameplayBackgroundAuxP = &gameplayBackgroundAux;
			Image* playerTAuxP = &playerTAux;
			Image* charactersBarsTAuxP = &charactersBarsTAux;
			Image* jewelsExplotionAuxP = &jewelsExplotionAux;
			Image* pauseBackgroundAuxP = &pauseBackgroundAux;
			Image* hitAuxP = &hitAux;
			Image* planetsTAuxP[typesOfPlanets];
			Image* enemiesTAuxP[typesOfEnemies];

			for (short i = 0; i < typesOfPlanets; i++)
			{
				planetsTAuxP[i] = &planetsTAux[i];
			}

			for (short i = 0; i < typesOfEnemies; i++)
			{
				enemiesTAuxP[i] = &enemiesTAux[i];
			}

			//width
			playerT.width = screenWidth / 3.29f;
			charactersBarsT.width = screenWidth / 3.29f;
			jewelsExplotion.width = board[0][0].dimentions.width * 5.0f;
			hit.width = playerT.width * 3.0f;

			for (short i = 0; i < typesOfPlanets; i++)
			{
				planetsT[i].width = board[0][0].dimentions.width;
			}

			enemiesT[static_cast<int>(ENEMIES::MONSTER1)].width = screenWidth / 2.67f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER2)].width = screenWidth / 3.2f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER3)].width = screenWidth / 3.33f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER4)].width = screenWidth / 3.0f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER5)].width = screenWidth / 2.77f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER6)].width = screenWidth / 4.46f;

			//height
			playerT.height = screenHeight / 4.54f;
			charactersBarsT.height = screenHeight / 8.0f;
			jewelsExplotion.height = board[0][0].dimentions.height * 4.0f;
			hit.height = playerT.height * 3.5f;

			for (short i = 0; i < typesOfPlanets; i++)
			{
				planetsT[i].height = board[0][0].dimentions.height;
			}

			enemiesT[static_cast<int>(ENEMIES::MONSTER1)].height = screenHeight / 4.54f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER2)].height = screenHeight / 4.43f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER3)].height = screenHeight / 4.4f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER4)].height = screenHeight / 4.6f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER5)].height = screenHeight / 4.89f;
			enemiesT[static_cast<int>(ENEMIES::MONSTER6)].height = screenHeight / 4.29f;

			//resizing
			ImageResize(gameplayBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));
			ImageResize(playerTAuxP, static_cast<int>(playerT.width), static_cast<int>(playerT.height));
			ImageResize(charactersBarsTAuxP, static_cast<int>(charactersBarsT.width), static_cast<int>(charactersBarsT.height));
			ImageResize(jewelsExplotionAuxP, static_cast<int>(jewelsExplotion.width), static_cast<int>(jewelsExplotion.height));
			ImageResize(pauseBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));
			ImageResize(hitAuxP, static_cast<int>(hit.width), static_cast<int>(hit.height));

			for (short i = 0; i < typesOfPlanets; i++)
			{
				ImageResize(planetsTAuxP[i], static_cast<int>(planetsT[i].width), static_cast<int>(planetsT[i].height));
			}

			for (short i = 0; i < typesOfEnemies; i++)
			{
				ImageResize(enemiesTAuxP[i], static_cast<int>(enemiesT[i].width), static_cast<int>(enemiesT[i].height));
			}

			//transformation from image* to image 
			gameplayBackgroundAux = *gameplayBackgroundAuxP;
			playerTAux = *playerTAuxP;
			charactersBarsTAux = *charactersBarsTAuxP;
			jewelsExplotionAux = *jewelsExplotionAuxP;
			pauseBackgroundAux = *pauseBackgroundAuxP;
			hitAux = *hitAuxP;

			for (short i = 0; i < typesOfPlanets; i++)
			{
				planetsTAux[i] = *planetsTAuxP[i];
			}

			for (short i = 0; i < typesOfEnemies; i++)
			{
				enemiesTAux[i] = *enemiesTAuxP[i];
			}

			//transformation from image to texture2D
			gameplayBackground.texture = LoadTextureFromImage(gameplayBackgroundAux);
			playerT.texture = LoadTextureFromImage(playerTAux);
			charactersBarsT.texture = LoadTextureFromImage(charactersBarsTAux);
			jewelsExplotion.texture = LoadTextureFromImage(jewelsExplotionAux);
			pauseBackground.texture = LoadTextureFromImage(pauseBackgroundAux);
			hit.texture = LoadTextureFromImage(hitAux);

			for (short i = 0; i < typesOfPlanets; i++)
			{
				planetsT[i].texture = LoadTextureFromImage(planetsTAux[i]);
			}

			for (short i = 0; i < typesOfEnemies; i++)
			{
				enemiesT[i].texture = LoadTextureFromImage(enemiesTAux[i]);
			}
		}

		void loadRulesTextures()
		{
			//images
			Image rulesBackgroundAux = LoadImage("res/assets/textures/rulesBackground.png");

			//images to images with pointer to resize
			Image* rulesBackgroundAuxP = &rulesBackgroundAux;

			//width

			//height

			//resizing
			ImageResize(rulesBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));

			//transformation from image* to image 
			rulesBackgroundAux = *rulesBackgroundAuxP;

			//transformation from image to texture2D
			rulesBackground.texture = LoadTextureFromImage(rulesBackgroundAux);
		}

		void loadCreditsTextures()
		{
			//images
			Image creditsBackgroundAux = LoadImage("res/assets/textures/creditsBackground.png");

			//images to images with pointer to resize
			Image* creditsBackgroundAuxP = &creditsBackgroundAux;

			//width

			//height

			//resizing
			ImageResize(creditsBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));

			//transformation from image* to image 
			creditsBackgroundAux = *creditsBackgroundAuxP;

			//transformation from image to texture2D
			creditsBackground.texture = LoadTextureFromImage(creditsBackgroundAux);
		}
	}
}
#ifndef EVENTS_H
#define EVENTS_H

namespace game
{
	namespace events
	{
		extern bool selectOption;
		extern bool enterPressed;
		extern bool losingMatch;
		extern bool losingMatch2;
		extern bool winningMatch;
		extern bool winningMatch2;
		extern bool jewelIsSelected;
		extern bool jewelsEliminated;
		extern bool damageDone;
		extern bool monsterHurt;
		extern bool playerHurt;
		extern bool inMenu;
		extern bool inGameplay;
		extern bool mute;
	
		void deactivateEvents();
	}
}

#endif // EVENTS_H
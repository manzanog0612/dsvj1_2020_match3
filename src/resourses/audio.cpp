#include "audio.h"

#include "events.h"

using namespace game;
using namespace events;

namespace game
{
	namespace audio
	{
		Sound click;
		Sound enter;
		Sound youLose;
		Sound losing;
		Sound youWin;
		Sound winning;
		Sound selectingJewel;
		Sound jewelsExplosion;
		Sound monsterGrunt;
		Sound playerGrunt;
		Sound punch;
		Music gameplayStream;
		Music menuStream;

		bool soundPLayed = false;
		bool sound2PLayed = false;
		bool soundMenuPlayed = false;
		
		void loadAudio()
		{
			click = LoadSound("res/assets/audio/sounds/click.mp3");
			enter = LoadSound("res/assets/audio/sounds/enter.mp3");
			youLose = LoadSound("res/assets/audio/sounds/youLose.mp3");
			losing = LoadSound("res/assets/audio/sounds/losing.mp3");
			youWin = LoadSound("res/assets/audio/sounds/youWin.mp3");
			winning = LoadSound("res/assets/audio/sounds/winning.mp3");
			selectingJewel = LoadSound("res/assets/audio/sounds/selectingJewel.mp3");
			jewelsExplosion = LoadSound("res/assets/audio/sounds/jewelsExplosion.mp3");
			monsterGrunt = LoadSound("res/assets/audio/sounds/monsterGrunt.mp3");
			playerGrunt = LoadSound("res/assets/audio/sounds/playerGrunt.mp3");
			punch = LoadSound("res/assets/audio/sounds/punch.mp3");
			gameplayStream = LoadMusicStream("res/assets/audio/music/gameplay.mp3");
			menuStream = LoadMusicStream("res/assets/audio/music/menu.mp3");

		}

		void playSound()
		{
			if (!mute && selectOption)		PlaySoundMulti(click);
			if (!mute && enterPressed)		PlaySoundMulti(enter);
			if (!mute && losingMatch)		PlaySound(losing);
			if (!mute && losingMatch2)		PlaySound(youLose);
			if (!mute && winningMatch)		PlaySound(winning);
			if (!mute && winningMatch2)		PlaySound(youWin);
			if (!mute && jewelIsSelected)	PlaySoundMulti(selectingJewel);
			if (!mute && jewelsEliminated)	PlaySoundMulti(jewelsExplosion);
			if (!mute && monsterHurt)		PlaySoundMulti(monsterGrunt);
			if (!mute && playerHurt)		PlaySoundMulti(playerGrunt);
			if (!mute && damageDone)		PlaySoundMulti(punch);

			SetSoundVolume(click, 0.5f);
			SetSoundVolume(enter, 0.5f);
			SetSoundVolume(losing, 0.1f);
			SetSoundVolume(youLose, 0.8f);
			SetSoundVolume(winning, 0.1f);
			SetSoundVolume(youWin, 0.8f);
			SetSoundVolume(selectingJewel, 0.8f);
			SetSoundVolume(jewelsExplosion, 0.5f);
			SetSoundVolume(monsterGrunt, 0.6f);
			SetSoundVolume(playerGrunt, 0.8f);
			SetSoundVolume(punch, 0.8f);
		}

		void playMusic()
		{
			if (inMenu) PlayMusicStream(menuStream);
			if (inGameplay) PlayMusicStream(gameplayStream);

			SetMusicVolume(menuStream, 0.1f);
			SetMusicVolume(gameplayStream, 0.1f);
		}

	}
}
#include "resourses/events.h"

namespace game
{
	namespace events
	{
		bool selectOption = false;
		bool enterPressed = false;
		bool losingMatch = false;
		bool losingMatch2 = false;
		bool winningMatch = false;
		bool winningMatch2 = false;
		bool jewelIsSelected = false;
		bool jewelsEliminated = false; 
		bool damageDone = false;
		bool monsterHurt = false;
		bool playerHurt = false;
		bool inMenu = false;
		bool inGameplay = false;
		bool mute = false;

		void deactivateEvents()
		{
			if (losingMatch)		losingMatch = false;
			if (losingMatch2)		losingMatch2 = false;
			if (winningMatch)		winningMatch = false;
			if (winningMatch2)		winningMatch2 = false;
			if (enterPressed)		enterPressed = false;
			if (jewelIsSelected)	jewelIsSelected = false;
			if (jewelsEliminated)	jewelsEliminated = false;
			if (damageDone)			damageDone = false;
			if (monsterHurt)		monsterHurt = false;
			if (playerHurt)			playerHurt = false;
		}
	}
}
#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace game
{
	namespace audio
	{
		extern Sound click;
		extern Sound enter;
		extern Sound youLose;
		extern Sound losing; 
		extern Sound youWin;
		extern Sound winning;
		extern Sound selectingJewel;
		extern Sound jewelsExplosion;
		extern Sound monsterGrunt;
		extern Sound playerGrunt;
		extern Sound punch;
		extern Music gameplayStream;
		extern Music menuStream;

		extern bool soundPLayed;
		extern bool sound2PLayed;
		extern bool soundMenuPlayed;

		void loadAudio();

		void playSound();
		void playMusic();
	}
}

#endif // AUDIO_H

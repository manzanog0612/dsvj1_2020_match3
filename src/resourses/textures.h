#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"

#include "game_vars/global_vars.h"

using namespace global_vars;

namespace game
{
	namespace textures
	{
		struct Textures
		{
			Texture2D texture;
			float width;
			float height;
		};

		extern Textures menuBackground;
		extern Textures gameplayBackground;
		extern Textures rulesBackground;
		extern Textures planetsT[typesOfPlanets];
		extern Textures enemiesT[typesOfEnemies];
		extern Textures playerT;
		extern Textures charactersBarsT;
		extern Textures jewelsExplotion;
		extern Textures pauseBackground;
		extern Textures arrowNext;
		extern Textures arrowPrevious;
		extern Textures creditsBackground;
		extern Textures hit;

		void loadMenuTextures();

		void loadGameplayTextures();

		void loadRulesTextures();

		void loadCreditsTextures();
	}
}

#endif // TEXTURES_H

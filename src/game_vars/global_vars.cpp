#include "game_vars/global_vars.h"

#include "elements/elements.h"
#include "game_vars/global_drawing_vars.h"

using namespace game;
using namespace elements;
using namespace global_drawing_vars;

namespace game
{
	namespace global_vars
	{
		short fps = 60;

		PLACEINGAME currentPlaceInGame;
		PLACEINGAME futurePlaceInGame;

		Mouse mouse;

		bool menuOptionChosen = false;

		bool returnToMenu = false;

		bool pauseGame = false;

		bool restart = false;

		short contourLineThickness = static_cast<short>(10 * drawScaleX);

		bool lose;

		bool win;

		short screenResolutionChoice = 2;

		float playerSpeed;

		bool mouseDown = false;

		Rectangle gameSpace;

		bool playingGame = true;
	}
}

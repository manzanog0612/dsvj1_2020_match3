#ifndef GLOBAL_VARS_H
#define GLOBAL_VARS_H

#include "raylib.h"

#include "elements/elements.h"
#include "configurations/configurations.h"

using namespace game;
using namespace elements;
using namespace configurations;

namespace game
{
	namespace global_vars
	{
		const short typesOfPlanets = 8;
		const short typesOfEnemies = 6;

		extern short fps;

		extern PLACEINGAME currentPlaceInGame;
		extern PLACEINGAME futurePlaceInGame;

		extern Mouse mouse;

		extern bool menuOptionChosen;

		extern bool returnToMenu;

		extern bool pauseGame;

		extern bool restart;

		extern short contourLineThickness;

		extern bool lose;

		extern bool win;

		extern short screenResolutionChoice;

		extern bool mouseDown;

		extern Rectangle gameSpace;

		const short maxBarsPoint = 50;

		extern bool playingGame;
	}
}
#endif //GLOBAL_VARS_H
